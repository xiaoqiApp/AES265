//
//  DESEncrypt.h
//
//  Created by isaced on 14/11/7.
//  Copyright (c) 2014 Year isaced. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  DES加密 
 *  使用了 iOS7+ NSData 提供的 Base-64
 *  解决了与 Java 平台加解密不一致问题
 */
@interface DESEncrypt : NSObject

/**
 *  DES加密
 *
 *  @param plainText 待加密的源文本
 *  @param key       密钥
 *
 *  @return 加密后的字符串
 */
+ (NSString *) encryptUseDES:(NSString *)plainText key:(NSString *)key;

/**
 *  DES解密
 *
 *  @param cipherText 待解密的密文
 *  @param key        密钥
 *
 *  @return 解密后的原字符串
 */
+ (NSString *) decryptUseDES:(NSString*)cipherText key:(NSString*)key;

@end
