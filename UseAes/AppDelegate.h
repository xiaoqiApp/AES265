//
//  AppDelegate.h
//  UseAes
//
//  Created by gitBurning on 14/11/6.
//  Copyright (c) 2014年 gitBurning. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

